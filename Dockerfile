FROM node:14-alpine as builder

WORKDIR /app
COPY . .
RUN npm install
RUN npm run tsc
RUN npm run lint


FROM node:14-alpine

WORKDIR /app

COPY --from=builder ./app/dist/src /app
COPY package* /app/
RUN npm install --production

EXPOSE 8080

ENTRYPOINT [ "node", "index.js" ]

HEALTHCHECK --interval=10s --timeout=3s --retries=3 \
  CMD wget --quiet --tries=1 --spider --no-check-certificate https://localhost:8080/.well-known/jwks.json \
   || wget --quiet --tries=1 --spider http://localhost:8080/.well-known/jwks.json \
   || exit 1