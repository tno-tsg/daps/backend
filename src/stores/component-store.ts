'use strict';
import { load } from 'js-yaml';
import MongoClient, {
  UpdateResult, Document, DeleteResult
} from 'mongodb';
import { pki, util as forgeutil } from 'node-forge';

import { ComponentInternal, Component } from '../model';
import { connectMongoDB } from '../util/mongo';
import { Configuration } from '../util/configuration';

const configuration = Configuration.getInstance();

/** Component Store for persisting component entries */
export class ComponentStore {
  /** Subject Key Identifier OID */
  private readonly subjectKeyIdentifier = '2.5.29.14';
  /** Authority Key Identifier OID */
  private readonly authorityKeyIdentifier = '2.5.29.35';
  /** MongoDB component database name */
  private readonly dbName = 'daps';

  /** Singleton instance */
  private static instance: ComponentStore;

  /** MongoDB component collection */
  private collection: MongoClient.Collection<ComponentInternal>;

  /** Get or create singleton instance */
  static getInstance (): ComponentStore {
    if (!ComponentStore.instance) {
      ComponentStore.instance = new ComponentStore();
      ComponentStore.instance.init();
    }

    return ComponentStore.instance;
  }

  /** Generate IDS ID from certificate */
  pemToId (pem: string): string {
    const certificate = pki.certificateFromPem(pem);
    const skiExtension = certificate.extensions.find(
      (extension) => extension.id === this.subjectKeyIdentifier
    );
    const akiExtension = certificate.extensions.find(
      (extension) => extension.id === this.authorityKeyIdentifier
    );

    if (!skiExtension || !akiExtension) {
      return '';
    }

    const ski = skiExtension.subjectKeyIdentifier
      .match(/.{1,2}/g)
      .join(':')
      .toUpperCase();
    const aki = forgeutil
      .bytesToHex(akiExtension.value)
      .match(/.{2}/g)
      .splice(4)
      .join(':')
      .toUpperCase();
    return `${ski}:keyid:${aki}`;
  }

  /** Initialize ComponentStore */
  async init (): Promise<void> {
    try {
      const client = await connectMongoDB();
      this.collection = client.db(this.dbName).collection('components');
      const count = await this.collection.countDocuments();

      if (count > 0) {
        console.log(
          `[ComponentStore] Collection not empty, not initializing with ${Configuration.fileLocation(
            'INITIAL_COMPONENTS'
          )}`
        );
        return;
      }
      if (configuration.initial.components === undefined) {
        console.log(
          `[ComponentStore] No initial components to load, ${Configuration.fileLocation(
            'INITIAL_COMPONENTS'
          )} does not exist`
        );
        return;
      }
      await this.insertInitialState();
    } catch (err) {
      console.log(
        `[ComponentStore] Error in requesting document count: ${err}`
      );
    }
  }

  /** Insert initial state of Components in configuration into the MongoDB collection */
  private async insertInitialState (): Promise<void> {
    const initialKeys: ComponentInternal[] = load(
      configuration.initial.components
    ) as ComponentInternal[];
    initialKeys.forEach((entry) => {
      entry.subject = this.pemToId(entry.certificate);
      entry.owner = 'admin';
    });
    console.log(
      '[ComponentStore] Inserting initial component state to database'
    );
    await this.collection.insertMany(initialKeys);
  }

  /** Retrieve components for internal use */
  public async list (): Promise<ComponentInternal[]> {
    return this.collection?.find({})?.toArray() || [];
  }

  /** Retrieve components for public use */
  public async listPublic (): Promise<Component[]> {
    const list = await this.list();
    return list.map((e) => {
      return {
        name: e.name,
        idsid: e.idsid,
        participant: e.participant,
        contact: e.contact,
        subject: e.subject,
        certificate: e.certificate,
        claim: e.claim,
        owner: e.owner
      };
    });
  }

  /** Retrieve a component with a given subject */
  public async get (subject: string): Promise<ComponentInternal | null> {
    return await this.collection?.findOne({ subject: subject });
  }

  /** Replace a component with a given subject */
  public replaceSubject (
    subject: string,
    component: ComponentInternal
  ): Promise<UpdateResult | Document> {
    return this.collection?.replaceOne({ subject: subject }, component, {
      upsert: true
    });
  }

  /** Delete a component with a given subject */
  public deleteSubject (subject: string): Promise<DeleteResult> {
    return this.collection?.deleteOne({ subject: subject });
  }

  /** Update the participant certification entries in components */
  public async certifyParticipant (
    participant: string,
    certify: boolean,
    level: string,
    date: Date
  ): Promise<UpdateResult | Document> {
    return this.collection?.updateMany(
      { participant: participant },
      {
        $set: {
          'claim.participantCertification.active': certify,
          'claim.participantCertification.level': level,
          'claim.participantCertification.lastActive': date
        }
      }
    );
  }

  /** Update the component certification entries */
  public async certifyComponent (
    subject: string,
    certify: boolean,
    securityProfile: string
  ): Promise<UpdateResult | Document> {
    return this.collection?.updateMany(
      { subject: subject },
      {
        $set: {
          'claim.componentCertification.active': certify,
          'claim.componentCertification.securityProfile': securityProfile,
          'claim.componentCertification.lastActive': new Date()
        }
      }
    );
  }
}
