import { md, pki } from 'node-forge';
import { CertificateAuthority } from '../model';
import crypto from 'crypto';

/** Construct X509 extensions for a given CA (for AKI fingerprinting) and CSR certificate (for additional X509 extensions (SAN & ext key usage)) */
function x509Extensions (
  ca: CertificateAuthority,
  certificate: pki.CertificateRequest
): any[] {
  const extensions: any[] = [
    {
      name: 'basicConstraints',
      cA: false
    },
    {
      name: 'keyUsage',
      keyCertSign: false,
      digitalSignature: true,
      nonRepudiation: false,
      keyEncipherment: true,
      dataEncipherment: false
    },
    {
      name: 'subjectKeyIdentifier'
    },
    {
      name: 'authorityKeyIdentifier',
      keyIdentifier: ca.getFingerprint()
    }
  ];

  if (certificate.getAttribute({ name: 'extensionRequest' })) {
    if (certificate.getAttribute({ name: 'extensionRequest' }).extensions) {
      const extensionRequest = certificate.getAttribute({
        name: 'extensionRequest'
      }).extensions;
      const subjectAltName = extensionRequest.find(
        (r) => r.name === 'subjectAltName'
      );
      const extKeyUsage = extensionRequest.find(
        (r) => r.name === 'extKeyUsage'
      );
      if (subjectAltName) {
        extensions.push(subjectAltName);
      }
      if (extKeyUsage) {
        extensions.push(extKeyUsage);
      }
    }
  }

  return extensions;
}

/** Create signed X.509 certificate for a given Certificate Signing Request and Certificate Authority */
export function createSignedCertificate (
  csr: string,
  ca: CertificateAuthority
): string {
  const forgecsr = pki.certificationRequestFromPem(csr) as pki.CertificateRequest;
  const cert = pki.createCertificate();
  cert.publicKey = forgecsr.publicKey;
  cert.serialNumber = `00${crypto.randomBytes(4).toString('hex')}`;
  cert.validity.notBefore = new Date();
  cert.validity.notAfter = new Date();
  cert.validity.notAfter.setFullYear(cert.validity.notBefore.getFullYear() + 2);
  cert.setSubject(forgecsr.subject.attributes);
  cert.setIssuer(ca.certificate.subject.attributes);

  const extensions = x509Extensions(ca, forgecsr);
  cert.setExtensions(extensions);

  cert.sign(ca.key, md.sha256.create());
  return pki.certificateToPem(cert);
}
