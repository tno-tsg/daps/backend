'use strict';

import * as fs from 'fs';
import { CertificateAuthority } from '../model';

/** Configuration class containing all configurable properties of the DAPS */
export class Configuration {
  /** Singleton configuration instance */
  private static instance: Configuration;

  /** Mapping from environment variable keys to default file location */
  static defaultFileLocations: { [key: string]: string } = {
    DAPS_KEY: '/secrets/identity/daps.key',
    DAPS_CERT: '/secrets/identity/daps.cert',
    CA_CHAIN: '/secrets/identity/cachain.cert',
    INITIAL_COMPONENTS: '/conf/components.yaml',
    INITIAL_PARTICIPANTS: '/conf/participants.yaml',
    INITIAL_FEDERATED_DAPS: '/conf/federateddaps.yaml',
    DEVICE_SUB_CA_KEY: '/secrets/ca/devicesubca.key',
    DEVICE_SUB_CA_CERT: '/secrets/ca/devicesubca.cert',
    PARTICIPANT_SUB_CA_KEY: '/secrets/ca/participantsubca.key',
    PARTICIPANT_SUB_CA_CERT: '/secrets/ca/participantsubca.cert'
  };

  /** Validate incoming client credentials */
  strict = process.env.STRICT.toLowerCase() !== 'false';

  /** Token issuer, must be in line with the domain the DAPS is hosted on */
  issuer = process.env.ISSUER;

  /** Enable DAPS federation */
  dapsFederation = process.env.DAPS_FEDERATION?.toLowerCase() === 'true';

  /** DAPS federation JWKS refresh interval (in milliseconds) */
  dapsFederationInterval = Number(
    process.env.DAPS_FEDERATION_INTERVAL || '86400000'
  );

  /** Use DAPS certificate for TLS encryption of the HTTP server */
  useHttps = process.env.PROTOCOL.toLowerCase() === 'https';

  /** Administration password in BCrypt format */
  adminPassword =
    process.env.ADMIN_PASSWORD ||
    '$2b$12$eJnNA51U6DEAfaZ3PzCK4uu3ILdPO7THZ17onYmJHnxRK5ly1pcJ2';

  /** MongoDB configuration */
  mongo = {
    hostname: process.env.MONGO_HOSTNAME,
    port: process.env.MONGO_PORT || 27017,
    username: process.env.MONGO_USERNAME,
    password: process.env.MONGO_PASSWORD,
    sslEnabled: process.env.MONGO_SSL_ENABLED.toLowerCase() === 'true'
  };

  /** Use email validation */
  emailValidation = process.env.EMAIL_VALIDATION?.toLowerCase() !== 'false';
  /** Title of the DAPS, as used in email communication */
  title = process.env.TITLE || 'TSG Identity Provider';
  /** Title of the dataspace, as used in email communication */
  dataspace = process.env.DATASPACE || 'TSG Dataspace';
  /** Location of the logo for this DAPS size should be 600px wide, as used in email communication */
  logo = process.env.LOGO || `${this.issuer}/logo.png`;

  /** SMTP Server for sending emails */
  smtpServer = process.env.SMTP_SERVER || 'in-v3.mailjet.com';
  /** SMTP Server port for sending emails */
  smtpPort = parseInt(process.env.SMTP_PORT) || 465;
  /** SMTP TLS enabled for sending emails */
  smtpSecure = process.env.SMTP_TLS?.toLowerCase() !== 'false';
  /** Mailjet SMTP API key for sending emails */
  smtpUser = process.env.SMTP_USER;
  /** Mailjet SMTP Secret key for sending emails */
  smtpPassword = process.env.SMTP_PASS;
  /** Mailjet SMTP Secret key for sending emails */
  smtpFrom = process.env.SMTP_FROM || 'noreply@dataspac.es';

  /** Automatically verify new certificates */
  autoHandleCertRequests = process.env.AUTO_HANDLE_CERT_REQUESTS?.toLowerCase() === 'true';

  /** DAPS private key, for signing tokens and optionally HTTPS */
  key = this.readFile('DAPS_KEY');
  /** DAPS certificate, for token validation */
  cert = this.readFile('DAPS_CERT');
  /** Certificate Authority Chain, for the DAPS certificate */
  CAChain = this.readFile('CA_CHAIN');

  /** Initial state configuration */
  initial = {
    components: this.readFile('INITIAL_COMPONENTS'),
    participants: this.readFile('INITIAL_PARTICIPANTS'),
    federatedDaps: this.dapsFederation
      ? this.readFile('INITIAL_FEDERATED_DAPS')
      : undefined
  };

  /** Certificate Authority key-pairs */
  ca = {
    device: new CertificateAuthority(
      this.readFile('DEVICE_SUB_CA_KEY'),
      this.readFile('DEVICE_SUB_CA_CERT')
    ),
    participant: new CertificateAuthority(
      this.readFile('PARTICIPANT_SUB_CA_KEY'),
      this.readFile('PARTICIPANT_SUB_CA_CERT')
    )
  };

  /** Retrieve singleton instance */
  static getInstance (): Configuration {
    if (!Configuration.instance) {
      Configuration.instance = new Configuration();
    }

    return Configuration.instance;
  }

  /** Get file location for a given key, either from environment variables or default locations */
  static fileLocation (key: string): string {
    return process.env[key] || this.defaultFileLocations[key];
  }

  /** Optionally read a file based  */
  private readFile (key: string): string | undefined {
    try {
      return fs.readFileSync(Configuration.fileLocation(key), 'utf8');
    } catch {
      return undefined;
    }
  }
}
