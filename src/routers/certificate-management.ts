import express, { RequestHandler, Router } from 'express';
import { ComponentStore } from '../stores/component-store';
import { ParticipantStore } from '../stores/participant-store';
import { Configuration } from '../util/configuration';
import { User, UserRole } from '../model';
import { createSignedCertificate } from '../util/pki';
import passport from 'passport';

export const router: Router = express.Router();

const configuration = Configuration.getInstance();
const componentStore = ComponentStore.getInstance();
const participantStore = ParticipantStore.getInstance();

/** Handle the request for the CA chain used by this DAPS instance */
router.get('/ca', async (req, res) => {
  res.send(configuration.CAChain);
});

/** Request handler ensuring a user is logged in */
const ensureAuthenticated: RequestHandler = passport.authenticate(
  'jwt',
  { session: false }
);

/** Request handler ensuring the current user has the ADMIN role */
const ensureAdmin: RequestHandler = (req, res, next) => {
  const user = req.user as User;
  if (user.role !== UserRole.ADMIN) {
    return res.status(403).send('');
  }
  next();
};

/** Handle the request for participants, either only of the current user or all participants in case the current user is an admin */
router.get('/participants', ensureAuthenticated, async (req, res) => {
  const user = req.user as User;
  const participants = await participantStore.list();
  if (user.role === UserRole.ADMIN) {
    res.send(participants);
  } else {
    res.send(participants.filter((p) => p.owner === user.username));
  }
});

/** Handle the request for components, either only of the current user or all components in case the current user is an admin */
router.get('/components', ensureAuthenticated, async (req, res) => {
  const user = req.user as User;
  const components = await componentStore.list();
  if (user.role === UserRole.ADMIN) {
    res.send(components);
  } else {
    res.send(components.filter((c) => c.owner === user.username));
  }
});

/** Create a new participant certificate based on a Certificate Signing Request and an identifier for the participant */
router.post('/participants', ensureAuthenticated, async (req, res) => {
  const user = req.user as User;
  const csr = req.body.csr.toString();

  const certificate = createSignedCertificate(
    csr,
    configuration.ca.participant
  );
  await participantStore.replace(req.body.id, {
    id: req.body.id,
    contact: user.name,
    email: user.email,
    certificate: certificate,
    certification: configuration.autoHandleCertRequests
      ? {
          active: true,
          lastActive: new Date(),
          level: 'Base'
        }
      : {
          active: false
        },
    owner: user.username
  });
  res.send({
    certificate: certificate.toString()
  });
});

/** Create a new component cetificate based on a Certificate Signing Request and name, id, and participant */
router.post('/components', ensureAuthenticated, async (req, res) => {
  const user = req.user as User;
  const csr = req.body.csr.toString();

  const participant = await participantStore.get(req.body.participant);
  if (participant === null) {
    res.send({
      error: 'Cannot find corresponding participant'
    });
    return;
  } else if (participant.owner !== user.username) {
    res.send({
      error: "You're not the owner of the participant certificate"
    });
  }
  const certificate = createSignedCertificate(csr, configuration.ca.device);
  const subject = componentStore.pemToId(certificate);
  console.log(`[CSRHandler] Identifier: ${subject}`);
  await componentStore.replaceSubject(subject, {
    name: req.body.name,
    idsid: req.body.id,
    participant: req.body.participant,
    contact: user.name,
    email: user.email,
    subject: subject,
    certificate: certificate.toString(),
    claim: {
      componentCertification: configuration.autoHandleCertRequests
        ? {
            active: true,
            lastActive: new Date(),
            securityProfile: 'Base'
          }
        : {
            active: false
          },
      participantCertification: {
        active: participant.certification.active,
        lastActive: participant.certification.lastActive,
        level: participant.certification.level
      }
    },
    owner: user.username
  });
  res.send({
    subject: subject,
    certificate: certificate.toString()
  });
});

/** Delete the participant certificate */
router.delete('/participants/:id', ensureAuthenticated, async (req, res) => {
  const user = req.user as User;
  const participant = await participantStore.get(req.params.id);
  if (user.role !== UserRole.ADMIN && participant.owner !== user.username) {
    return res.status(403).send('');
  }
  try {
    await participantStore.delete(req.params.id);
    const components = await componentStore.list();
    const componentFutures = components
      .filter((c) => c.participant === participant.id)
      .map(async (c) => {
        await componentStore.deleteSubject(c.subject);
      });
    await Promise.all(componentFutures);
    res.send('');
  } catch (e) {
    res.status(500).send(e);
  }
});

/** Delete the component certificate */
router.delete('/components/:subject', ensureAuthenticated, async (req, res) => {
  const user = req.user as User;
  const component = await componentStore.get(req.params.subject);
  if (user.role !== UserRole.ADMIN && component.owner !== user.username) {
    return res.status(403).send('');
  }
  try {
    await componentStore.deleteSubject(req.params.subject);
    res.send('');
  } catch (e) {
    res.status(500).send(e);
  }
});

/** Set the certification for a given participant */
router.post(
  '/participants/certify',
  ensureAuthenticated,
  ensureAdmin,
  async (req, res) => {
    const participant = req.body.participant;
    const certify = req.body.certify;
    const level = req.body.level;
    try {
      await participantStore.certifyParticipant(participant, certify, level);
      res.send('');
    } catch (e) {
      res.status(500).send(e);
    }
  }
);

/** Set the certification for a given component */
router.post(
  '/components/certify',
  ensureAuthenticated,
  ensureAdmin,
  async (req, res) => {
    const subject = req.body.subject;
    const certify = req.body.certify;
    const securityProfile = req.body.securityProfile;
    try {
      await componentStore.certifyComponent(subject, certify, securityProfile);
      res.send('');
    } catch (e) {
      res.status(500).send(e);
    }
  }
);
